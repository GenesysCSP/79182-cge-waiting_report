import React, { useEffect, Fragment } from 'react'
import { useLocation, useHistory } from 'react-router-dom'
import { getParameterByName } from '../services/utils'
import config from '../config';
import { validateSubscription } from '../services/utils'
import { getOrganization } from '../services/purecloud'

const Main = () => {
    const location = useLocation()
    const history = useHistory()

    useEffect(() => {
        const purecloudInitialization = async () => {
            let env = new URLSearchParams(location.search).get('environment')
            if (!env) env = 'mypurecloud.com'
            sessionStorage.setItem('purecloud-csp-env', env)
            console.log('Main.purecloud.init:', env)

            if (getParameterByName('access_token')) sessionStorage.setItem('purecloud-csp-token', getParameterByName('access_token'))
            if (sessionStorage.getItem('purecloud-csp-token')) {
                try {
                    const organization = await getOrganization(sessionStorage.getItem('purecloud-csp-env'), sessionStorage.getItem('purecloud-csp-token'))
                    await validateSubscription(organization.id, 'INSERT-APP-ID')
                    history.push('/home')
                } catch (error) {
                    history.push('/unauthorized', error.message)
                }
            }
            else history.push('/login')
        }
        const pureconnectInitialization = () => {
            if (sessionStorage.getItem('pureconnect-csrf-token')) {
                history.push('/home')
            } else {
                history.push('/login')
            }
        }
        switch (config.product) {
            case 'purecloud': purecloudInitialization()
                break;
            case 'pureconnect': pureconnectInitialization()
                break;
            case 'pureengage':
                break;
            default:
                break;
        }
        // eslint-disable-next-line
    }, [])

    return (<Fragment></Fragment>)
}

export default Main