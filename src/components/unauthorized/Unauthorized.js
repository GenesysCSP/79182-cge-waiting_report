import React from 'react'
import { Alert } from 'reactstrap'

const Unauthorized = (props) => {
    return (<Alert color="danger">{props.location.state}</Alert>)
}

export default Unauthorized